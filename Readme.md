<!-- Readme.md is generated from Readme.org. Please edit that file -->


# paperplanes R data package

This package contains distance recordings from a paper plane
folding/flying experiment.

Visitors of a science fair (the [3. 'Nacht des Wissens'](http://www.ndw.uni-goettingen.de) in
Göttingen/Germany) had the opportunity to throw a paper plane.  The
data was recorded and the visitors had the possibility to see some live
statistics of their performance as well as some overall
comparisons.

The planes were thrown along a narrow corridor with a high ceiling.  There
was an open door at a distance of 14m after which the corridor continued at
a 'normal' height.

## Installation

This package is on [cran](https://cran.r-project.org/package=paperplanes), so install via

    install.packages("paperplanes")

## Loading the package

Load the package via

    library("paperplanes")

## The Data

Loading the packages will make the data.frame (actually a tibble) `paperplanes`
available.  Here are the first 6 entries:

    head(paperplanes)

<table border="2" cellspacing="0" cellpadding="6" rules="groups" frame="hsides">


<colgroup>
<col  class="org-right" />

<col  class="org-left" />

<col  class="org-left" />

<col  class="org-left" />

<col  class="org-right" />

<col  class="org-left" />

<col  class="org-right" />

<col  class="org-right" />
</colgroup>
<thead>
<tr>
<th scope="col" class="org-right">id</th>
<th scope="col" class="org-left">hour</th>
<th scope="col" class="org-left">person</th>
<th scope="col" class="org-left">gender</th>
<th scope="col" class="org-right">age</th>
<th scope="col" class="org-left">plane</th>
<th scope="col" class="org-right">paper</th>
<th scope="col" class="org-right">distance</th>
</tr>
</thead>

<tbody>
<tr>
<td class="org-right">1</td>
<td class="org-left">[17,18)</td>
<td class="org-left">Roland</td>
<td class="org-left">male</td>
<td class="org-right">30</td>
<td class="org-left">Standard80</td>
<td class="org-right">80</td>
<td class="org-right">7.8</td>
</tr>


<tr>
<td class="org-right">2</td>
<td class="org-left">[17,18)</td>
<td class="org-left">Astrid</td>
<td class="org-left">female</td>
<td class="org-right">30</td>
<td class="org-left">Concorde120</td>
<td class="org-right">120</td>
<td class="org-right">2.7</td>
</tr>


<tr>
<td class="org-right">3</td>
<td class="org-left">[17,18)</td>
<td class="org-left">Roland</td>
<td class="org-left">male</td>
<td class="org-right">30</td>
<td class="org-left">Standard120</td>
<td class="org-right">120</td>
<td class="org-right">9.2</td>
</tr>


<tr>
<td class="org-right">4</td>
<td class="org-left">[17,18)</td>
<td class="org-left">Isabella</td>
<td class="org-left">female</td>
<td class="org-right">48</td>
<td class="org-left">Standard120</td>
<td class="org-right">120</td>
<td class="org-right">6</td>
</tr>


<tr>
<td class="org-right">5</td>
<td class="org-left">[17,18)</td>
<td class="org-left">Fabienne</td>
<td class="org-left">female</td>
<td class="org-right">17</td>
<td class="org-left">Standard120</td>
<td class="org-right">120</td>
<td class="org-right">7.3</td>
</tr>


<tr>
<td class="org-right">6</td>
<td class="org-left">[17,18)</td>
<td class="org-left">Fabienne</td>
<td class="org-left">female</td>
<td class="org-right">17</td>
<td class="org-left">Standard120</td>
<td class="org-right">120</td>
<td class="org-right">7.8</td>
</tr>
</tbody>
</table>

The columns are:

-   id column
-   hour of the day for that flight (the event went on from 5pm till midnight)
-   gender of the pilot
-   age of the pilot (in years)
-   plane: name of the plane
-   paper: two types of paper have been available: 80 g/m² and 120 g/m²
-   distance in meters

The data has 615 rows which
correspond to 615  flights.

These 615 flights were done with
177 paper planes thrown by
173 (66
female and 107 male) pilots.

## Statistics Primer Using Paper Planes

### Gender

Having that kind of data at hand one (obvious?) question to ask is:
Is there a gender effect?  Or more provocative:

> Do male pilots achieve longer distances?

The easiest way to look at this question is to average the distances
from all flights done by male pilots and compare that mean distance to
that of all flights done by female pilots:

    meandistance_by_gender <-
      paperplanes %>%
      group_by(gender) %>%
      summarize(meandistance = mean(distance))

<table border="2" cellspacing="0" cellpadding="6" rules="groups" frame="hsides">


<colgroup>
<col  class="org-left" />

<col  class="org-right" />
</colgroup>
<thead>
<tr>
<th scope="col" class="org-left">gender</th>
<th scope="col" class="org-right">meandistance</th>
</tr>
</thead>

<tbody>
<tr>
<td class="org-left">female</td>
<td class="org-right">5.42</td>
</tr>


<tr>
<td class="org-left">male</td>
<td class="org-right">7.64</td>
</tr>
</tbody>
</table>

So flights by male participants went roughly two meters further than
flights by female pilots.

The natural next question is:  do we see that by chance?  Or: how
certain can we be that there truly is a difference?  Or even more
specific:

> How big has the observed distance to be so that we are convinced that
> we do not see a difference just by chance?

When would you believe that there is a true difference?  If the
observed difference was only 10cm, we'd probably claim, this is by
chance.  But are the observed 2m enough?

To really answer that question, one important piece of information is
still missing:  How much do the distances vary (in statistical
terminology: what is the *variance* of the distances?).  To clarify
this point, let's look at three versions of the data:

![img](Readme_three_variances.png)

The plot shows three versions of the data:  the center panel shows the
observed data; on the left the variances have been artificially
increased; on the right the variances have been artificially
decreased.  The effect is a decrease in the spread of the data from
the left to the right.  Please note that the mean distance per gender (indicated
by the horizontal lines) is the same in all three situations
meaning that the difference of the means is always identical.  In
which situation though would you believe in a real effect?

Let's look at the same data again, this time zooming in.

![img](Readme_three_variances_freescale.png)

It is clearly visible that in the situation with the small variances
(the right panel) most of the blue dots appear above most of the red
dots so that we might believe in a true effect here.  In
contrast, in the situation with the large variances (the left panel)
the ranges of the dots basically overlap so that we might not be
convinced to see something else than a difference by chance.

More statistically sound:  The more data points we have the better we
are in estimating the mean.  Here is the data again.  This time
the mean estimate is a big dot complemented by the [confidence interval](https://en.wikipedia.org/wiki/Confidence_interval):

![img](Readme_three_variances_cis.png)

(Here we show the 99.999% confidence intervals, as only then, we
actually **see** some intervals&#x2026;)

The confidence intervals for the mean estimate contain the true value
of the mean with a 99.999% probability.  If the two confidence intervals (the one for the mean distance
from flights by male pilots and the one for the mean distance from
flights by female pilots) do not overlap, we can say with the same
(99.999%) confidence that the two means are different.

From the above plot we can, thus, conclude that in the case of the
large variances, the difference is not statistically significant,
while in the other cases (observed variances and small variances) the
difference is statistically significant.

Roughly, this is what the  [t-test](https://en.wikipedia.org/wiki/T_test)  does:  Intuitively, you could say
it checks, whether the confidence intervals around mean estimates in
two groups overlap.
The t-test is one representative of statistical tests,
that try to address the question:

> How can we quantify our level of believe?

Typically, statistical tests will assume that in reality there is no effect
and give an estimate on how likely it is to see an effect at least as
large as the observed one.

In our case this means:  If male and female pilots achieve similar
distances in reality, how likely is it to see &#x2013; just by chance &#x2013; a difference of roughly two
meters or even more in a data set of 615 flights
(213 from female pilots and
402 from male pilots) with the observed
variability in distances.

In R one way to perform a t-test is:

    ## extract the distances per gender
    distances_male   <- paperplanes %>% filter(gender == "male")   %>% .$distance
    distances_female <- paperplanes %>% filter(gender == "female") %>% .$distance

    ## perform the t-test
    t.test(distances_male, distances_female)

    	Welch Two Sample t-test

    data:  distances_male and distances_female
    t = 8.4941, df = 535.76, p-value < 2.2e-16
    alternative hypothesis: true difference in means is not equal to 0
    95 percent confidence interval:
     1.708041 2.735747
    sample estimates:
    mean of x mean of y
     7.642786  5.420892

This test gives

-   the means in both groups (bottom line)
-   an interval where the difference is expected:
    we are 95% confident that the difference is greater than 1.7 meter
    but not more than 2.7 meter
-   the hypothesis
-   and the values of the statistical test

The *p-value* is given to be
1.98e-16
which translates to
0.000000000000000198.
Thus, it is highly unlikely to see that data if there was no gender
effect.  In reverse we are very confident that there is a
gender effect.

### Gender &#x2013; re-done

Let's briefly recap:  We have seen that there is a statistically
significant difference in the distance from flights by male pilots
and the distance from flights by female pilots.  The p-value of that
comparison was very small &#x2013; way below the usually accepted 5% level.

So, why should we re-do that analysis?

Well, in the overly simple analysis above we ignored the dependencies
within the data.  What are these dependencies?  Let us assume for a
moment that all flights had been done by only two pilots: one male
pilot and one female pilot.  In such case would you believe in any
gender effect visible in the data?  Or would you rather assume that
just by chance the male pilot might have been a good pilot and the
female a weak pilot?  How could we tell these two things apart?

The key idea here is to model the data allowing for pilots with
different strengths.  The technicalities of such so called [mixed
effect models](https://en.wikipedia.org/wiki/Mixed_model) are beyond the scope of this report.

Here we just present how to fit such models in R

    library("lme4")
    mod <- lmer(distance ~ gender + (1 | plane) + (1 | person),
                data = paperplanes)

The first argument to the `lmer` function is a `formula` specifying
the model to fit to the data.  In this case we want to model (think:
predict) the `distance` given the `gender` of the pilot while we account
for differences among planes and pilots (persons).

We are interested in the parameter the model assigns to `gender`.  To
visualize this we use the following code:

    library("sjPlot")
    modplot <- sjp.lmer(mod,
                        type = "fe",
                        show.values = FALSE,
                        show.p = FALSE,
                        show.intercept = TRUE,
                        p.kr = FALSE,
                        prnt.plot = FALSE)

    modplot$plot +
      theme_fivethirtyeight() +
      theme(title = element_blank(),
            panel.grid.major.y = element_blank())

![img](Readme_model_gender.png)

The numbers are given in the following table:

    modplot %>%
      .$data %>%
      dplyr::select(term, estimate, conf.low, conf.high, p.value) %>%
      mutate_if(is.numeric, funs(todigits2))

<table border="2" cellspacing="0" cellpadding="6" rules="groups" frame="hsides">


<colgroup>
<col  class="org-left" />

<col  class="org-right" />

<col  class="org-right" />

<col  class="org-right" />

<col  class="org-right" />
</colgroup>
<thead>
<tr>
<th scope="col" class="org-left">term</th>
<th scope="col" class="org-right">estimate</th>
<th scope="col" class="org-right">conf.low</th>
<th scope="col" class="org-right">conf.high</th>
<th scope="col" class="org-right">p.value</th>
</tr>
</thead>

<tbody>
<tr>
<td class="org-left">(Intercept)</td>
<td class="org-right">5.4</td>
<td class="org-right">4.6</td>
<td class="org-right">6.1</td>
<td class="org-right">7.4e-47</td>
</tr>


<tr>
<td class="org-left">gendermale</td>
<td class="org-right">2.3</td>
<td class="org-right">1.4</td>
<td class="org-right">3.2</td>
<td class="org-right">1.2e-06</td>
</tr>
</tbody>
</table>

The interpretation is straightforward:  According to this model, the
average distance is 5.4 meters.  If the gender is `male` the model
adds 2.3 meters (which is the modeled gender effect).

The (95%) confidence interval around that gendermale factor is wider than in
the oversimple model above, but still does not contain 0, so that we
are confident that there is a positive additional factor for male
pilots.

### Paperweight

Planes could be built using either heavy (120 g / m²) or light (80 g /
m²) paper.  So, we can perform an analogous analysis comparing the two
paperweights as we did above comparing male and female pilots.

Let's first visualise all flights again, this time grouped by paper
weight:

![img](Readme_paperweights.png)

A simple t-test between flights using a plane with heavy and flights
using a plane with light paper shows a statistically significant
difference:

    ## extract the distances per gender
    distances_80  <- paperplanes %>% filter(paper == 80)  %>% .$distance
    distances_120 <- paperplanes %>% filter(paper == 120) %>% .$distance

    ## perform the t-test
    t.test(distances_80, distances_120)

    	Welch Two Sample t-test

    data:  distances_80 and distances_120
    t = -4.5435, df = 586.12, p-value = 6.721e-06
    alternative hypothesis: true difference in means is not equal to 0
    95 percent confidence interval:
     -1.7842831 -0.7072656
    sample estimates:
    mean of x mean of y
     6.129839  7.375613

But we already know by now, that we ought to account for the
dependencies in the data, so let's fit another mixed effects model:

    mod <- paperplanes %>%
      mutate(paper = as.factor(paper)) %>%
      lmer(distance ~ paper + (1 | plane) + (1 | person),
           data = .)

The resulting estimates are plotted again

![img](Readme_model_paper.png)

Here are the numbers:

    modplot %>%
      .$data %>%
      dplyr::select(term, estimate, conf.low, conf.high, p.value) %>%
      mutate_if(is.numeric, funs(todigits2))

<table border="2" cellspacing="0" cellpadding="6" rules="groups" frame="hsides">


<colgroup>
<col  class="org-left" />

<col  class="org-right" />

<col  class="org-right" />

<col  class="org-right" />

<col  class="org-right" />
</colgroup>
<thead>
<tr>
<th scope="col" class="org-left">term</th>
<th scope="col" class="org-right">estimate</th>
<th scope="col" class="org-right">conf.low</th>
<th scope="col" class="org-right">conf.high</th>
<th scope="col" class="org-right">p.value</th>
</tr>
</thead>

<tbody>
<tr>
<td class="org-left">(Intercept)</td>
<td class="org-right">6.0</td>
<td class="org-right">5.35</td>
<td class="org-right">6.7</td>
<td class="org-right">1.1e-67</td>
</tr>


<tr>
<td class="org-left">paper120</td>
<td class="org-right">1.3</td>
<td class="org-right">0.44</td>
<td class="org-right">2.1</td>
<td class="org-right">2.8e-03</td>
</tr>
</tbody>
</table>

The model is similar to the gender model above.  And there seems
to be a statistically significant effect of the paper
weight:  planes made from heavier (120 g/m²) paper went 1.3 meter
further according to this model.

### Paperweight re-done

Let's recap again.  We have seen how to test for differences in two
groups accounting for the dependencies we see.  Both gender and paper
have a significant influence on the distance.  Are we done here?  Not
quite yet.

What if all male pilots went for the heavy paper and all female
pilots for the light paper?

Maybe we should quickly check this:

![img](Readme_papergender.png)

So there **is** a problem:  Female pilots preferred the light paper
(ratio 8:5) while male pilots preferred the heavy paper (ratio 2:5).
This might have been enforced by the fact, that for easier distinction
the colour of the paper was different: the heavier paper was light
blue, the lighter paper was light red.

What are the consequences?  Due to this, when we compare light to heavy
paper, we simultaneously to some extent compare female to male pilots.
This leads to the question:  Is there a real gender effect and is
there a real paper weight effect?  Or is one only driven by the other?

The way to address this question is a joint model, where both, paper
weight and gender are included as predictors.

By now, we know how to fit such models in R:

    mod <- paperplanes %>%
      mutate(paper = as.factor(paper)) %>%
      lmer(distance ~ gender + paper + (1 | plane) + (1 | person),
           data = .)

The resulting estimates are plotted again

![img](Readme_model_genderpaper.png)

Here are the numbers:

    modplot %>%
      .$data %>%
      dplyr::select(term, estimate, conf.low, conf.high, p.value) %>%
      mutate_if(is.numeric, funs(todigits2))

<table border="2" cellspacing="0" cellpadding="6" rules="groups" frame="hsides">


<colgroup>
<col  class="org-left" />

<col  class="org-right" />

<col  class="org-right" />

<col  class="org-right" />

<col  class="org-right" />
</colgroup>
<thead>
<tr>
<th scope="col" class="org-left">term</th>
<th scope="col" class="org-right">estimate</th>
<th scope="col" class="org-right">conf.low</th>
<th scope="col" class="org-right">conf.high</th>
<th scope="col" class="org-right">p.value</th>
</tr>
</thead>

<tbody>
<tr>
<td class="org-left">(Intercept)</td>
<td class="org-right">5.1</td>
<td class="org-right">4.31</td>
<td class="org-right">5.9</td>
<td class="org-right">1.9e-36</td>
</tr>


<tr>
<td class="org-left">gendermale</td>
<td class="org-right">2.02</td>
<td class="org-right">1.05</td>
<td class="org-right">3.0</td>
<td class="org-right">4.2e-05</td>
</tr>


<tr>
<td class="org-left">paper120</td>
<td class="org-right">0.72</td>
<td class="org-right">-0.14</td>
<td class="org-right">1.6</td>
<td class="org-right">9.9e-02</td>
</tr>
</tbody>
</table>

In this model, male pilots are still achieving statistically
significantly longer distances (on average 2.02 meters).  The heavier
paper also leads to longer distances (on average 0.71 meter) but this
is not statistically significant at the 5% level.  This means that at
the commonly accepted 5% level we cannot confidently exclude to see
that difference just by chance.

As a last point in this section, we'll look into the paper weight
induced differences within each gender:

![img](Readme_weightbygender.png)

### Outro

We presented the data from a paper plane throwing contest and did
some first analyses.

It is left to the reader to look deeper into the data.  We did not
look into any interactions.  And we have not looked into the other
variables in the data set: checking for
possible further interactions with age might be interesting, too.
Another point to take into account would be potential learning effects
of pilots in subsequent throws.

We'll round off this introduction of the data with two more plots.

![img](Readme_agebygender.png)

![img](Readme_agebyhour.png)


<div class="markdown">
<!&#x2013;
Local Variables:
 mode: gfm
 markdown-command: "marked"
End:
&#x2013;>

</div>